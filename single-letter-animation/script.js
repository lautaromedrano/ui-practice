// On load, split text into individual spans
document.addEventListener('DOMContentLoaded', (event) => {
    let textDiv = document.querySelector('#animatedText');
    let text = textDiv.innerText;
    
    textDiv.innerText = '';
    text.split('').forEach((letter, index) => {
        let letterSpan = document.createElement("span");

        letterSpan.id = `letter${index}`;
        letterSpan.innerText = letter;
        
        letterSpan.addEventListener('mouseover', (event) => { letterSpan.className = 'animateIn' });
        letterSpan.addEventListener('mouseout', (event) => { letterSpan.className = 'animateOut' });
        
        textDiv.appendChild(letterSpan);
        setTimeout(() => { letterSpan.className = 'animateIn'; }, index * 25);
        setTimeout(() => { letterSpan.className = 'animateOut'; }, 250 + index * 25);
    });
});